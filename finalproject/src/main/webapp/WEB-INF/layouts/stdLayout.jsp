<!DOCTYPE html>
<%@ include file="/WEB-INF/layouts/include.jsp"%>
<html lang="en" xml:lang="en" xmlns= "http://www.w3.org/1999/xhtml">
<head>
	<meta charset="UTF-8">
	<meta name="google" content="notranslate">
	<meta http-equiv="Content-Language" content="en">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><tiles:getAsString name="title" /></title>
	<link rel="shortcut icon" href="<c:url value='/resources/img/favicon.ico' />">
	<!-- Bootstrap import - Check https://ui.oreillyauto.com/ui to make sure you have the latest! -->
	<link rel="stylesheet" href="<c:url value='/resources/css/oreillybs-4.0.0r1.min.css' />">
	<!-- Project CSS import -->
	<link type="text/css" href="<c:url value='/resources/css/master.css' />" rel="stylesheet" />
	<!-- O'Reilly JS -->
	<script src="<c:url value='/resources/js/oreillyjs/1.1.40/orly.js' />"></script>
</head>
<body>
	<div id="bodyContentTile" class="container">
		<div class="row">
			<div class="col-sm-12">
				<orly-alert-mgr id="alerts"></orly-alert-mgr>
				<tiles:insertAttribute name="body" />
			</div>
		</div>
	</div>
</body>
</html>
